# download_raster

Note: This README is currently under construction. Additional information and details will be added soon. Thank you for your understanding.

## Getting started

This QGIS plugin enables easy and efficient downloading of raster images from Google Earth Engine (GEE), simplifying the integration of global geospatial data into your QGIS projects.


## Installation
Before installing this plugin, it is necessary to pre-install the Google Earth Engine plugin. This plugin will be automatically installed at the time of installing 'download_raster' plugin.


## Usage


## Support

## Contributing



